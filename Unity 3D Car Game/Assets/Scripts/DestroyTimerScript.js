﻿#pragma strict

var destroyAfter : float = 10;
var timer : float;

function Start () {

}

function Update () {

    timer += Time.deltaTime;
    if(destroyAfter <= timer) {
        Destroy(gameObject);
    }

}