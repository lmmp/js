﻿#pragma strict

var wheelFL : WheelCollider;
var wheelFR : WheelCollider;
var wheelRL : WheelCollider;
var wheelRR : WheelCollider;

var wheelFLTrans : Transform;
var wheelFRTrans : Transform;
var wheelRLTrans : Transform;
var wheelRRTrans : Transform;

var maxTorque : float = 1000;
var steeringAngle : float = 20;
var centerOfMass : float = -0.1;
var decelerationSpeed : float = 300;
var currentSpeed : float;
var gearRatio : int[];

var backLightObject : GameObject;
var idleLightMaterial : Material;
var brakeLightMaterial : Material;
var reverseLightMaterial : Material;

var braked : boolean = false;
var maxBrakeTorque : float = 2000;

var mySidewayFriction : float;
var myForwardFriction : float;
var slipSidewayFriction : float = 0.02;
var slipForwardFriction : float = 0.04;

var coinsCollected : int = 0;

var timer : float = 120.0;
var gameOver : boolean = false;
var highScore : int = 0;

function Start () {
    GetComponent.<Rigidbody>().centerOfMass.y = centerOfMass;
    SetValues();
    highScore = PlayerPrefs.GetInt("HiScore");
}

function SetValues() {

    myForwardFriction = wheelRR.forwardFriction.stiffness;
    mySidewayFriction= wheelRR.sidewaysFriction.stiffness;

}

function FixedUpdate () {

    currentSpeed = GetComponent.<Rigidbody>().velocity.magnitude*3.6;;

    wheelRR.motorTorque = maxTorque * Input.GetAxis("Vertical");
    wheelRL.motorTorque = maxTorque * Input.GetAxis("Vertical");

    if(Input.GetButton("Vertical") == false) {
        wheelRR.brakeTorque = decelerationSpeed;
        wheelRL.brakeTorque = decelerationSpeed;
    } else {
        wheelRR.brakeTorque = 0;
        wheelRL.brakeTorque = 0;
    }

    wheelFR.steerAngle = steeringAngle * Input.GetAxis("Horizontal");
    wheelFL.steerAngle = steeringAngle * Input.GetAxis("Horizontal");

    HandBrake();
    Restart();

}

function Update() {

    wheelFLTrans.Rotate(wheelFL.rpm/60*360*Time.deltaTime,0,0);
    wheelFRTrans.Rotate(wheelFR.rpm/60*-360*Time.deltaTime,0,0);
    wheelRLTrans.Rotate(wheelRL.rpm/60*360*Time.deltaTime,0,0);
    wheelRRTrans.Rotate(wheelRR.rpm/60*-360*Time.deltaTime,0,0);

    wheelFLTrans.localEulerAngles.y = wheelFL.steerAngle - wheelFLTrans.localEulerAngles.z;
    wheelFRTrans.localEulerAngles.y = wheelFR.steerAngle + 180 - wheelFRTrans.localEulerAngles.z;

    BackLight();
    WheelPosition();
    EngineSound();
    if(timer > 0) {
        timer -= Time.deltaTime;
    } else {
        gameOver = true;
        timer = 0;
        var oldScore = highScore;
        var newScore = coinsCollected;
        if(newScore > oldScore) {
            PlayerPrefs.SetInt("HiScore",newScore);
        } else {
            PlayerPrefs.SetInt("HiScore",oldScore);
        }

    }


}

function OnGUI() {

    if(!gameOver) {
        GUI.Box(new Rect(10, 10, 130, 20), "Time Left: " + timer.ToString("0"));
    } else {
        GUI.Box(new Rect(10, 10, 130, 20), "Game Over (R)");
    }

    GUI.Box(new Rect(10, 40, 130, 20), "Coins Collected: " + coinsCollected);

    GUI.Box(new Rect(10, 70, 130, 20), "High Score: " + highScore);

}

function BackLight() {

    if(currentSpeed > 0 && Input.GetAxis("Vertical") < 0 && !braked) {
        backLightObject.GetComponent.<Renderer>().material = brakeLightMaterial;
    } else if(currentSpeed < 0 && Input.GetAxis("Vertical") > 0 && !braked) {
        backLightObject.GetComponent.<Renderer>().material = brakeLightMaterial;
    } else if(currentSpeed < 0 && Input.GetAxis("Vertical") < 0 && !braked) {
        backLightObject.GetComponent.<Renderer>().material = reverseLightMaterial;
    } else {
        backLightObject.GetComponent.<Renderer>().material = idleLightMaterial;
    }

}

function WheelPosition() {
    
    var hit : RaycastHit;
    var wheelPos : Vector3;

    if(Physics.Raycast(wheelFL.transform.position, -wheelFL.transform.up, hit, wheelFL.radius+wheelFL.suspensionDistance)) {
        wheelPos = hit.point+wheelFL.transform.up * wheelFL.radius;
    } else {
        wheelPos = wheelFL.transform.position - wheelFL.transform.up * wheelFL.suspensionDistance;
    }
    wheelFLTrans.position = wheelPos;

    if(Physics.Raycast(wheelFR.transform.position, -wheelFR.transform.up, hit, wheelFR.radius+wheelFR.suspensionDistance)) {
        wheelPos = hit.point+wheelFR.transform.up * wheelFL.radius;
    } else {
        wheelPos = wheelFR.transform.position - wheelFR.transform.up * wheelFR.suspensionDistance;
    }
    wheelFRTrans.position = wheelPos;

    if(Physics.Raycast(wheelRL.transform.position, -wheelRL.transform.up, hit, wheelRL.radius+wheelRL.suspensionDistance)) {
        wheelPos = hit.point+wheelRL.transform.up * wheelRL.radius;
    } else {
        wheelPos = wheelRL.transform.position - wheelRL.transform.up * wheelRL.suspensionDistance;
    }
    wheelRLTrans.position = wheelPos;

    if(Physics.Raycast(wheelRR.transform.position, -wheelRR.transform.up, hit, wheelRR.radius+wheelRR.suspensionDistance)) {
        wheelPos = hit.point+wheelRR.transform.up * wheelFL.radius;
    } else {
        wheelPos = wheelRR.transform.position - wheelRR.transform.up * wheelRR.suspensionDistance;
    }
    wheelRRTrans.position = wheelPos;

}

function HandBrake() {

    if(Input.GetButton("Jump")) {
        braked = true;
    } else {
        braked = false;
    }

    if(braked) {

        wheelRR.brakeTorque = maxBrakeTorque;
        wheelRL.brakeTorque = maxBrakeTorque;
        wheelRR.motorTorque = 0;
        wheelRL.motorTorque = 0;

        SetSlip(slipForwardFriction, slipSidewayFriction);

    } else {

        SetSlip(myForwardFriction, mySidewayFriction);

    }

}

function SetSlip(currentForwardFriction : float, currentSidewayFriction : float) {

    wheelRR.forwardFriction.stiffness = currentForwardFriction;
    wheelRL.forwardFriction.stiffness = currentForwardFriction;
    wheelFR.forwardFriction.stiffness = currentForwardFriction;
    wheelFL.forwardFriction.stiffness = currentForwardFriction;

    if(currentForwardFriction < 1) {
        wheelFR.forwardFriction.stiffness = currentForwardFriction + 0.2;
        wheelFL.forwardFriction.stiffness = currentForwardFriction + 0.2;
    }

    wheelRR.sidewaysFriction.stiffness = currentSidewayFriction;
    wheelRL.sidewaysFriction.stiffness = currentSidewayFriction;
    wheelFR.sidewaysFriction.stiffness = currentSidewayFriction;
    wheelFL.sidewaysFriction.stiffness = currentSidewayFriction;

    if(currentSidewayFriction < 1) {
        wheelFR.sidewaysFriction.stiffness = currentSidewayFriction + 0.2;
        wheelFL.sidewaysFriction.stiffness = currentSidewayFriction + 0.2;
    }

}

function OnTriggerEnter (other : Collider) 
{
    if(!gameOver) {
        if(other.transform.gameObject.name == "Coin(Clone)" || other.transform.gameObject.name == "Coin") {
            
            var audio = GetComponents.<AudioSource>()[1];

            audio.Play(); 
            
            Destroy(other.transform.gameObject);
            coinsCollected++;
        }
    }
}

function Restart() {
    
    if(gameOver) {
        if(Input.GetKeyDown("r")) {
            Application.LoadLevel (Application.loadedLevelName);
        }
    }

}

function EngineSound() {

    var i = 0;

    for(i = 0; i < gearRatio.length; i++) {
        
        if(gearRatio[i] > currentSpeed) {
            break;
        }

    }

    var gearMinValue : float = 0.0;
    var gearMaxValue : float = 0.0;

    if(i == 0) {
        gearMinValue = 0;
        gearMaxValue = gearRatio[i];
    } else {
        gearMinValue = gearRatio[i - 1];
        gearMaxValue = gearRatio[i];
    }

    var enginePitch : float = (currentSpeed - gearMinValue) / (gearMaxValue - gearMinValue);

    var inc : float = 0.8;

    if(i != 0) {
        enginePitch = enginePitch / 2;
        inc = 1.3;
    }

    var audio = GetComponents.<AudioSource>()[0];

    audio.pitch = enginePitch + inc; 

}