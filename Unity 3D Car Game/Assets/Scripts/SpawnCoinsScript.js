﻿#pragma strict

var coin : GameObject;
var spawnPosition;
var timer = 0.0;
var spawnInterval = 3;

var minZ = 140;
var maxZ = 320;

var minX = 160;
var maxX = 380;

var y = 0.8;

function SpawnCoin() {

    spawnPosition = Vector3(Random.Range(minX, maxX), 0.8, Random.Range(minZ, maxZ));
    Instantiate(coin, spawnPosition, Quaternion.Euler(90,0,0));

}

function Start () {

}

function Update () {

    timer += Time.deltaTime;

    if(timer > spawnInterval) {
        SpawnCoin();
        timer = 0.0;
    }

}