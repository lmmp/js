﻿#pragma strict

var currentFrictionValue : float;
var skidAt : float = 0.4;
var markWidth : float = 0.2;
var skidding : int;
var lastPos = new Vector3[2];
var soundEmission : float = 5;
private var soundWait : float;

var skidMaterial : Material;
var skidSmoke : GameObject;
var skidSound : GameObject;

function Start () {

}

function Update () {

    var hit : WheelHit;
    transform.GetComponent(WheelCollider).GetGroundHit(hit);
    currentFrictionValue = Mathf.Abs(hit.sidewaysSlip);
    
    if(skidAt <= currentFrictionValue) {
        skidSmoke.GetComponent.<ParticleEmitter>().emit = true;
        SkidMesh();
        if(soundWait <= 0) {
            Instantiate(skidSound, hit.point, Quaternion.identity);
            soundWait = 1;
        }
    } else {
        skidSmoke.GetComponent.<ParticleEmitter>().emit = false;
        skidding = 0;
    }

    soundWait -= Time.deltaTime * soundEmission;

}

function SkidMesh() {

    var hit : WheelHit;
    transform.GetComponent(WheelCollider).GetGroundHit(hit);

    var mark : GameObject = new GameObject("Mark");
    var filter : MeshFilter = mark.AddComponent(MeshFilter);
    mark.AddComponent(MeshRenderer);
    var markMesh : Mesh = new Mesh();
    var vertices = new Vector3[4];
    var triangles = new int[6];

    if(skidding == 0) {

        vertices[0] = hit.point + Quaternion.Euler(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.z)*Vector3(markWidth,0.01,0);
        vertices[1] = hit.point + Quaternion.Euler(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.z)*Vector3(-markWidth,0.01,0);
        vertices[2] = hit.point + Quaternion.Euler(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.z)*Vector3(-markWidth,0.01,0);
        vertices[3] = hit.point + Quaternion.Euler(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.z)*Vector3(markWidth,0.01,0);

        lastPos[0] = vertices[2];
        lastPos[1] = vertices[3];

        skidding = 1;

    } else {

        vertices[1] = lastPos[0];
        vertices[0] = lastPos[1];
        vertices[2] = hit.point + Quaternion.Euler(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.z)*Vector3(-markWidth,0.01,0);
        vertices[3] = hit.point + Quaternion.Euler(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.z)*Vector3(markWidth,0.01,0);

        lastPos[0] = vertices[2];
        lastPos[1] = vertices[3];

    }

    triangles = [0,1,2,0,2,3,0,3,2,0,2,1];

    markMesh.vertices = vertices;
    markMesh.triangles = triangles;
    markMesh.RecalculateNormals();

//    var uvm = new Vector2[markMesh.vertices.length];

    var uvm : Vector2[] = new Vector2[4];

    uvm[0] = Vector2(1,0);
    uvm[1] = Vector2(0,0);
    uvm[2] = Vector2(0,1);
    uvm[3] = Vector2(1,1);

//    for(var i = 0; i < uvm.length; i++) {
//        uvm[i] = Vector2(markMesh.vertices[i].x,markMesh.vertices[i].z);
//    }

    markMesh.uv = uvm;
    filter.mesh = markMesh;
    mark.GetComponent.<Renderer>().material = skidMaterial;
    mark.AddComponent(DestroyTimerScript);

}